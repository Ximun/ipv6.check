# ipv6 check

This simple script scans a list of domains name. It checks if each domain
retrieves AAAA record type, also for domains behind MX and NS results.

Then, the result is displayed on simple html page.

## Usage

Clone the project.

```
cd /path/to/directory/
git clone https://framagit.org/Ximun/ipv6.check.git
cd ipv6.check
```

Register hosts list to check into `conf.yaml`.

Exemple:

```
# conf.yaml
hosts:
  - ffdn.org
  - fdn.fr
  - aquilenet.fr
```

Launch main script

```
./run.sh
```

Fin the result on `./dist/index.html`.
