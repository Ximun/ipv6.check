#!/usr/bin/env python3

from typing import Dict, Any
import yaml
import asyncio
import aiodns
import os
import datetime

from jinja2 import Environment, FileSystemLoader, Template

conf_file = 'conf.yaml'
query_types = ["AAAA", "MX", "NS"]
results = {}

async def resolve(host, query_type):
  if query_type == "AAAA":
    results[host][query_type] = await query(host, query_type)
  elif query_type == "MX" or query_type == "NS":
    try:
      temp_result = await resolver.query(host, query_type)
    except aiodns.error.DNSError:
      temp_result = ""
    if len(temp_result) == 0:
      results[host][query_type] = False
    else:
      results[host][query_type] = all([await query(response.host, "AAAA") for response in temp_result])

async def query(name, query_type):
  try:
    result = await resolver.query(name, query_type)
  except aiodns.error.DNSError:
    return False
  if len(result) == 0:
    return False
  return True

def prepare_results(hosts: Dict[str, Any]):
  for host in hosts:
    results[host] = {}

async def main():
  conf = yaml.load(open(conf_file,'r'), Loader=yaml.BaseLoader)
  hosts = conf["hosts"]

  prepare_results(hosts)

  for query_type in query_types:
    await asyncio.wait([resolve(host, query_type) for host in hosts])

  jinja_env = Environment(loader=FileSystemLoader("templates/"))
  template = jinja_env.get_template("index.jinja2")

  with open(os.path.join("dist", "index.html"), "w") as fh:
    fh.write(
      template.render(
        date=datetime.datetime.now().strftime("%d %B %Y %H:%M"),
        query_types=query_types,
        results=results
      )
    )

if __name__ == "__main__":
  loop = asyncio.get_event_loop()
  resolver = aiodns.DNSResolver(loop=loop)
  loop.run_until_complete(main())
